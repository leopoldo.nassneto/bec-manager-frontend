# Manager frontend project

The frontend part of the TaskManager workflow.

#### Requisites to run
- NodeJS
- NPM
- NG
- The backend project

#### How to run
- Clone the project
- Check and adjust the file `proxy.config.js`, backend API proxy configuration
- Install packages, in a shell, on the root project folder, run `npm install`
- In a shell, on the root project folder, run `npm start`
- The project will run on `http://localhost:80`

#### How to run
- Clone the project
- Check and adjust the file `proxy.config.js`, backend API proxy configuration
- Install packages, in a shell, on the root project folder, run `npm install`
- In a shell, on the root project folder, run `npm start`
- The project will run on `http://localhost:80`

##### Apiary
- You can check the full apiary in the link `https://leopoldomanager.docs.apiary.io`

#### Next steps for evolution
- Login/Authentication
- Grid pagination
- Form validation
- Migrate actions to modal format
- Implement a better CORS Solution
- Analyze dependencies and remove unused
- Add loading icons