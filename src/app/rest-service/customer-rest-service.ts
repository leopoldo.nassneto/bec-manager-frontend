import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { RestService, endpoint, httpOptions } from '../rest.service';

@Injectable({
    providedIn: 'root'
  })
export class CustomerRestService extends RestService {
  
    getCustomers(): Observable<any> {
      return this.http.get(endpoint + 'customers').pipe(
        map(this.extractData));
    }
    
    getCustomer(id): Observable<any> {
      return this.http.get(endpoint + 'customer/' + id).pipe(
        map(this.extractData));
    }
    
    addCustomer (customer): Observable<any> {
      console.log(customer);
      return this.http.post<any>(endpoint + 'customers', JSON.stringify(customer), httpOptions).pipe(
        tap((customer) => console.log(`added customer w/ id=${customer.id}`)),
        catchError(this.handleError<any>('addCustomer'))
      );
    }
    
    updateCustomer (id, customer): Observable<any> {
      return this.http.put(endpoint + 'customer/' + id, JSON.stringify(customer), httpOptions).pipe(
        tap(_ => console.log(`updated customer id=${id}`)),
        catchError(this.handleError<any>('updateCustomer'))
      );
    }
    
    deleteCustomer (id): Observable<any> {
      return this.http.delete<any>(endpoint + 'customer/' + id, httpOptions).pipe(
        tap(_ => console.log(`deleted customer id=${id}`)),
        catchError(this.handleError<any>('deleteCustomer'))
      );
    }
  }