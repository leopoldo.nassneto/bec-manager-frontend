import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { RestService, endpoint, httpOptions } from '../rest.service';
import { CustomerRestService } from '../rest-service/customer-rest-service';
import { UserRestService } from '../rest-service/user-rest-service';
import { TicketHistoryRestService } from '../rest-service/ticket-history-rest-service';

@Injectable({
    providedIn: 'root'
  })
export class TicketRestService extends RestService {
  
    getTickets(status, technical): Observable<any> {
      var ep: String = '?';
      
      if (status) {
        ep = ep+'&status='+status;
      }
      if (technical) {
        ep = ep+'&technical='+technical;
      }
      return this.http.get(endpoint + 'tickets' + ep).pipe(
        map(this.extractData));
    }
    
    getTicket(id): Observable<any> {
      return this.http.get(endpoint + 'ticket/' + id).pipe(
        map(this.extractData));
    }

    prepareObjectToSend(ticket) {
        if (ticket.technical != null) {
          if (ticket.technical.id == null) {
            ticket.technical = {'id': ticket.technical}
          }
        }
        if (ticket.customer != null) {
          if (ticket.customer.id == null) { 
            ticket.customer = {'id': ticket.customer}
          }
        }
        return ticket;
    }
    
    addTicket (ticket): Observable<any> {
      return this.http.post<any>(endpoint + 'tickets', JSON.stringify(this.prepareObjectToSend(ticket)), httpOptions).pipe(
        tap((ticket) => console.log(`added ticket w/ id=${ticket.id}`)),
        catchError(this.handleError<any>('addTicket'))
      );
    }
    
    updateTicket (id, ticket): Observable<any> {
      return this.http.put(endpoint + 'ticket/' + id, JSON.stringify(this.prepareObjectToSend(ticket)), httpOptions).pipe(
        tap(_ => console.log(`updated ticket id=${id}`)),
        catchError(this.handleError<any>('updateTicket'))
      );
    }
    
    deleteTicket (id): Observable<any> {
      return this.http.delete<any>(endpoint + 'ticket/' + id, httpOptions).pipe(
        tap(_ => console.log(`deleted ticket id=${id}`)),
        catchError(this.handleError<any>('deleteTicket'))
      );
    }

    getAllCustomers(): Observable<any> {
      var customerRestService = new CustomerRestService(this.http);
      return customerRestService.getCustomers();
    }

    getAllUsers(): Observable<any> {
      var technicalRestService = new UserRestService(this.http);
      return technicalRestService.getUsers();
    }


    getTicketHistorys(ticket): Observable<any> {
      var ticketHistoryRestService = new TicketHistoryRestService(this.http);
      return ticketHistoryRestService.getTicketHistorys(ticket);
    }

    deleteTicketHistory (id): Observable<any> {
      var ticketHistoryRestService = new TicketHistoryRestService(this.http);
      return ticketHistoryRestService.deleteTicketHistory(id);
    }
    


  }