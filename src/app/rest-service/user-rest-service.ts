import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { RestService, endpoint, httpOptions } from '../rest.service';

@Injectable({
    providedIn: 'root'
  })
export class UserRestService extends RestService {
  
    getUsers(): Observable<any> {
      return this.http.get(endpoint + 'users').pipe(
        map(this.extractData));
    }
    
    getUser(id): Observable<any> {
      return this.http.get(endpoint + 'user/' + id).pipe(
        map(this.extractData));
    }
    
    addUser (user): Observable<any> {
      console.log(user);
      return this.http.post<any>(endpoint + 'users', JSON.stringify(user), httpOptions).pipe(
        tap((user) => console.log(`added user w/ id=${user.id}`)),
        catchError(this.handleError<any>('addUser'))
      );
    }
    
    updateUser (id, user): Observable<any> {
      return this.http.put(endpoint + 'user/' + id, JSON.stringify(user), httpOptions).pipe(
        tap(_ => console.log(`updated user id=${id}`)),
        catchError(this.handleError<any>('updateUser'))
      );
    }
    
    deleteUser (id): Observable<any> {
      return this.http.delete<any>(endpoint + 'user/' + id, httpOptions).pipe(
        tap(_ => console.log(`deleted user id=${id}`)),
        catchError(this.handleError<any>('deleteUser'))
      );
    }
  }