import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { RestService, endpoint, httpOptions } from '../rest.service';
import { UserRestService } from '../rest-service/user-rest-service';
import { TicketRestService } from '../rest-service/ticket-rest-service';

@Injectable({
    providedIn: 'root'
  })
export class TicketHistoryRestService extends RestService {
  
    getTicketHistorys(ticket): Observable<any> {
      return this.http.get(endpoint + 'tickets_history/?ticket/'+ticket).pipe(
        map(this.extractData));
    }
    
    getTicketHistory(id): Observable<any> {
      return this.http.get(endpoint + 'ticket_history/' + id).pipe(
        map(this.extractData));
    }
    
    addTicketHistory (ticket, ticket_history): Observable<any> {
      ticket_history.ticket = {"id": ticket};
      return this.http.post<any>(endpoint + 'tickets_history', JSON.stringify(this.prepareObjectToSend(ticket_history)), httpOptions).pipe(
        tap((ticket_history) => console.log(`added ticket history w/ id=${ticket_history.id}`)),
        catchError(this.handleError<any>('addTicketHistory'))
      );
    }
    
    updateTicketHistory (id, ticket_history): Observable<any> {
      return this.http.put(endpoint + 'ticket_history/' + id, JSON.stringify(this.prepareObjectToSend(ticket_history)), httpOptions).pipe(
        tap(_ => console.log(`updated ticket history id=${id}`)),
        catchError(this.handleError<any>('updateTicketHistory'))
      );
    }
    
    deleteTicketHistory (id): Observable<any> {
      return this.http.delete<any>(endpoint + 'ticket_history/' + id, httpOptions).pipe(
        tap(_ => console.log(`deleted ticket history id=${id}`)),
        catchError(this.handleError<any>('deleteTicketHistory'))
      );
    }
   
    prepareObjectToSend(ticketHistory) {
      if (ticketHistory.technical != null) {
        if (ticketHistory.technical.id == null) {
          ticketHistory.technical = {'id': ticketHistory.technical}
        }
      }
      if (ticketHistory.ticket != null) {
        if (ticketHistory.ticket.id == null) { 
          ticketHistory.ticket = {'id': ticketHistory.ticket}
        }
      } 
      return ticketHistory;
    }

    getAllUsers(): Observable<any> {
      var technicalRestService = new UserRestService(this.http);
      return technicalRestService.getUsers();
    }
   
    getTicket(ticketId): Observable<any> {
      var ticketRestService = new TicketRestService(this.http);
      return ticketRestService.getTicket(ticketId);
    }
  }