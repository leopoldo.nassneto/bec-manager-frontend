import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { UserComponent } from './user/list/user.component';
import { UserAddComponent } from './user/add/user-add.component';
import { UserDetailComponent } from './user/detail/user-detail.component';
import { UserEditComponent } from './user/edit/user-edit.component';

import { CustomerComponent } from './customer/list/customer.component';
import { CustomerEditComponent } from './customer/edit/customer-edit.component';
import { CustomerDetailComponent } from './customer/detail/customer-detail.component';
import { CustomerAddComponent } from './customer/add/customer-add.component';

import { TicketComponent } from './ticket/list/ticket.component';
import { TicketEditComponent } from './ticket/edit/ticket-edit.component';
import { TicketDetailComponent } from './ticket/detail/ticket-detail.component';
import { TicketAddComponent } from './ticket/add/ticket-add.component';

import { AlertsModule } from 'angular-alert-module';
import { TicketHistoryAddComponent } from './ticket-history/add/ticket-history-add.component';
import { TicketHistoryEditComponent } from './ticket-history/edit/ticket-history-edit.component';
import { TicketHistoryDetailComponent } from './ticket-history/detail/ticket-history-detail.component';

const appRoutes: Routes = [
  /* Users Routes */
  {
    path: 'users',
    component: UserComponent,
    data: { title: 'Users List' }
  },
  {
    path: 'user-details/:id',
    component: UserDetailComponent,
    data: { title: 'User Details' }
  },
  {
    path: 'user-add',
    component: UserAddComponent,
    data: { title: 'User Add' }
  },
  {
    path: 'user-edit/:id',
    component: UserEditComponent,
    data: { title: 'User Edit' }
  },
  /*Customers Routes*/
  {
    path: 'customers',
    component: CustomerComponent,
    data: { title: 'Customers List' }
  },
  {
    path: 'customer-details/:id',
    component: CustomerDetailComponent,
    data: { title: 'Customer Details' }
  },
  {
    path: 'customer-add',
    component: CustomerAddComponent,
    data: { title: 'Customer Add' }
  },
  {
    path: 'customer-edit/:id',
    component: CustomerEditComponent,
    data: { title: 'Customer Edit' }
  },
  /*Ticket Routes*/
  {
    path: 'tickets',
    component: TicketComponent,
    data: { title: 'Tickets List' }
  },
  {
    path: 'ticket-details/:id',
    component: TicketDetailComponent,
    data: { title: 'Ticket Details' }
  },
  {
    path: 'ticket-add',
    component: TicketAddComponent,
    data: { title: 'Ticket Add' }
  },
  {
    path: 'ticket-edit/:id',
    component: TicketEditComponent,
    data: { title: 'Ticket Edit' }
  },
  /*Ticket History Routes*/
  {
    path: 'ticket-history-details/:id',
    component: TicketHistoryDetailComponent,
    data: { title: 'Ticket Details' }
  },
  {
    path: 'ticket-history-add/:id',
    component: TicketHistoryAddComponent,
    data: { title: 'Ticket Add' }
  },
  {
    path: 'ticket-history-edit/:id',
    component: TicketHistoryEditComponent,
    data: { title: 'Ticket Edit' }
  },
  /* Main Route */
  { path: 'header',
    component: HeaderComponent,
    data: { title: 'Welcome' },
  },
  { path: '',
    redirectTo: '/tickets',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    /* User Components */
    UserComponent,
    UserAddComponent,
    UserDetailComponent,
    UserEditComponent,
    /* Customer Components */
    CustomerComponent,
    CustomerEditComponent,
    CustomerDetailComponent,
    CustomerAddComponent,
    /* Ticket Components */
    TicketComponent,
    TicketEditComponent,
    TicketDetailComponent,
    TicketAddComponent,
    /* Ticket History Components */
    TicketHistoryAddComponent,
    TicketHistoryEditComponent,
    TicketHistoryDetailComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AlertsModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
