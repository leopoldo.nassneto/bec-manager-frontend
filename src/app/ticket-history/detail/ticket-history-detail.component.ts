import { Component, OnInit } from '@angular/core';
import { MainComponent } from '../../general/ManagerMainComponent'
import { TicketHistoryRestService } from 'src/app/rest-service/ticket-history-rest-service';
import { TicketRestService } from '../../rest-service/ticket-rest-service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-ticket-history-detail',
  templateUrl: './ticket-history-detail.component.html',
  styleUrls: ['./ticket-history-detail.component.css']
})
export class TicketHistoryDetailComponent extends MainComponent {

  ticketHistory:any;

  constructor(public rest:TicketHistoryRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
    this.rest.getTicketHistory(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.ticketHistory = data;
    });
  }


  getTechnicalName(ticketHistory) {
    if (ticketHistory.technical!==null){
      return ticketHistory.technical.name
    } else {
      return ''
    }
    
  }

  getTicketHistoryKind(ticket) {
    switch(ticket.kind){
      case 1:
        return "Comment"
        break
      case 2:
        return "Working Progress"
        break
      default:
        return "Not Classified"
        break
    }
  }

}
