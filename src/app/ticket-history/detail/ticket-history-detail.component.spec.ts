import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketHistoryDetailComponent } from './ticket-history-detail.component';

describe('TicketHistoryDetailComponent', () => {
  let component: TicketHistoryDetailComponent;
  let fixture: ComponentFixture<TicketHistoryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketHistoryDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketHistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
