import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketHistoryAddComponent } from './ticket-history-add.component';

describe('TicketHistoryAddComponent', () => {
  let component: TicketHistoryAddComponent;
  let fixture: ComponentFixture<TicketHistoryAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketHistoryAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketHistoryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
