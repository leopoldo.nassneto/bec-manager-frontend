import { Component, OnInit, Input } from '@angular/core';
import { TicketHistoryRestService } from '../../rest-service/ticket-history-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-ticket-history-add',
  templateUrl: './ticket-history-add.component.html',
  styleUrls: ['./ticket-history-add.component.css']
})
export class TicketHistoryAddComponent extends MainComponent {

  @Input() ticketHistoryData = {  };

  constructor(
    public rest:TicketHistoryRestService,
    private route: ActivatedRoute,
    private router: Router) {
    super();
  }

  ticket:any;
  users:any = [];

  ngOnInit() {
    this.rest.getTicket(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.ticket = data;
      this.getUsers();
    });
  }

  getUsers() {
    this.users = [];
    this.rest.getAllUsers().subscribe((data: {}) => {
      this.users = data;
    });
  }

  checkIfNewTechnical(value) {
    if (value == '-1') {
      this.router.navigate(['/user-add/']);
    }
  }

  addTicketHistory() {
    this.rest.addTicketHistory(this.ticket.id, this.ticketHistoryData).subscribe((result) => {
      if (result.error) {
        this.alert(result.error.message);
      } else {
        this.router.navigate(['/ticket-history-details/'+result.id]);
      }
    }, (err) => {
      alert(err.message)
      console.log(err.message);
    });
  }

}