import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketHistoryEditComponent } from './ticket-history-edit.component';

describe('TicketHistoryEditComponent', () => {
  let component: TicketHistoryEditComponent;
  let fixture: ComponentFixture<TicketHistoryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketHistoryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketHistoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
