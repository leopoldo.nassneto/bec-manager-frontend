import { Component, OnInit, Input } from '@angular/core';
import { TicketHistoryRestService } from '../../rest-service/ticket-history-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-ticket-history-edit',
  templateUrl: './ticket-history-edit.component.html',
  styleUrls: ['./ticket-history-edit.component.css']
})
export class TicketHistoryEditComponent extends MainComponent {

  @Input() ticketHistoryData:any = { };

  users:any = [];

  constructor(public rest:TicketHistoryRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
    this.rest.getTicketHistory(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.ticketHistoryData = data;
      if (this.ticketHistoryData.technical != null) {
        this.ticketHistoryData.technical = this.ticketHistoryData.technical.id;
      }
    });
    this.getUsers();
  }

  getUsers() {
    this.users = [];
    this.rest.getAllUsers().subscribe((data: {}) => {
      console.log(data);
      this.users = data;
    });
  }

  updateTicketHistory() {
    this.rest.updateTicketHistory(this.route.snapshot.params['id'], this.ticketHistoryData).subscribe((result) => {
      if (result.error) {
        this.alert(result.error.message);
      } else {
        this.router.navigate(['/ticket-history-details/'+result.id]);
      }
    }, (err) => {
      console.log(err);
    });
  }

}