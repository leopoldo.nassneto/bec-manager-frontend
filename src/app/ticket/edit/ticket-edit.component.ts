import { Component, OnInit, Input } from '@angular/core';
import { TicketRestService } from '../../rest-service/ticket-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-ticket-edit',
  templateUrl: './ticket-edit.component.html',
  styleUrls: ['./ticket-edit.component.css']
})
export class TicketEditComponent extends MainComponent {

  @Input() ticketData:any = { };

  customers:any = [];
  users:any = [];

  constructor(public rest:TicketRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
    this.rest.getTicket(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.ticketData = data;
      if (this.ticketData.customer != null) {
        this.ticketData.customer = this.ticketData.customer.id;
      }
      if (this.ticketData.technical != null) {
        this.ticketData.technical = this.ticketData.technical.id;
      }
    });
    this.getCustomers();
    this.getUsers();
  }

  getCustomers() {
    this.customers = [];
    this.rest.getAllCustomers().subscribe((data: {}) => {
      console.log(data);
      this.customers = data;
    });
  }

  getUsers() {
    this.users = [];
    this.rest.getAllUsers().subscribe((data: {}) => {
      console.log(data);
      this.users = data;
    });
  }

  updateTicket() {
    this.rest.updateTicket(this.route.snapshot.params['id'], this.ticketData).subscribe((result) => {
      if (result.error) {
        this.alert(result.error.message);
      } else {
        this.router.navigate(['/ticket-details/'+result.id]);
      }
    }, (err) => {
      console.log(err);
    });
  }

}