import { Component, OnInit } from '@angular/core';
import { TicketRestService } from '../../rest-service/ticket-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent extends MainComponent {

  tickets:any = [];
  users:any = [];

  status: any;
  technical: any;



  constructor(public rest:TicketRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.status = params['status'];
      this.technical = params['technical'];
    });

    this.getTickets(this.status, this.technical);
    this.getUsers();
  }

  search() {
    var qp = {"queryParams": {"status": '-1', "technical": '-1'} };
    
    if (this.status) {
      qp.queryParams.status = this.status;
    }
    if (this.technical) {
      qp.queryParams.technical = this.technical;
    }

    this.router.navigate(['/tickets'], qp);
    window.location.reload();
  }

  clear() {
    this.router.navigate(['/tickets']);
    window.location.reload();
  }

  getTickets(status, technical) {
    this.tickets = [];
    this.rest.getTickets(status, technical).subscribe((data: {}) => {
      console.log(data)
      this.tickets = data
    })
  }

  getUsers() {
    this.users = [];
    this.rest.getAllUsers().subscribe((data: {}) => {
      console.log(data);
      this.users = data;
    });
  }

  getCustomerName(ticket) {
    if (ticket.customer!==null){
      return ticket.customer.name
    } else {
      return ''
    }
    
  }

  getTicketStatus(ticket) {
    switch(ticket.status){
      case 1:
        return "Queued"
        break
      case 2:
        return "Doing"
        break
      case 3:
        return "Blocked"
        break
      case 4:
        return "Done"
        break
      default:
        return "Not Classified"
        break
    }
  }

  getTechnicalName(ticket) {
    if (ticket.technical!==null){
      return ticket.technical.name
    } else {
      return ''
    }
    
  }

  add() {
    this.router.navigate(['/ticket-add']);
  }


  delete(id) {
    this.rest.deleteTicket(id)
      .subscribe(res => {
        if (res.error) {
          this.alert(res.error.message);
        } else {
            this.getTickets(this.status, this.technical);
          }
        }, (err) => {
          console.log(err);
        }
      );
  }

}