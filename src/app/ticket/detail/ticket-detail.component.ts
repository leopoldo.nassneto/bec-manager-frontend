import { Component, OnInit } from '@angular/core';
import { TicketRestService } from '../../rest-service/ticket-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent extends MainComponent {

  ticket:any;
  ticketHistorys:any = [];


  constructor(public rest:TicketRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  getCustomerName(ticket) {
    if (ticket.customer!==null){
      return ticket.customer.name
    } else {
      return ''
    }
    
  }
  getCustomerPhone(ticket) {
    if (ticket.customer!==null){
      return ticket.customer.phone
    } else {
      return ''
    }
    
  }
  getTicketStatus(ticket) {
    switch(ticket.status){
      case 1:
        return "Queued"
        break
      case 2:
        return "Doing"
        break
      case 3:
        return "Blocked"
        break
      case 4:
        return "Done"
        break
      default:
        return "Not Classified"
        break
    }
  }

  ticketHistoryDelete(id) {
    this.rest.deleteTicketHistory(id)
      .subscribe(res => {
        if (res.error) {
          this.alert(res.error.message);
        } else {
            this.getTicketsHistorys();
          }
        }, (err) => {
          console.log(err);
        }
      );
  }

  getTicketHistoryKind(ticket) {
    switch(ticket.kind){
      case 1:
        return "Comment"
        break
      case 2:
        return "Working Progress"
        break
      default:
        return "Not Classified"
        break
    }
  }

  getTechnicalName(ticket) {
    if (ticket.technical!==null){
      return ticket.technical.name
    } else {
      return ''
    }
    
  }

  
  getTicketsHistorys() {
    this.ticketHistorys = [];
    this.rest.getTicketHistorys(this.ticket.id).subscribe((data: {}) => {
      console.log(data);
      this.ticketHistorys = data
    })
  }

  ngOnInit() {
    this.rest.getTicket(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.ticket = data;
      this.getTicketsHistorys();
    });
  }

}