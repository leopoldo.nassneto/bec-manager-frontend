import { Component, OnInit, Input } from '@angular/core';
import { TicketRestService } from '../../rest-service/ticket-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-ticket-add',
  templateUrl: './ticket-add.component.html',
  styleUrls: ['./ticket-add.component.css']
})
export class TicketAddComponent extends MainComponent {

  @Input() ticketData = {};

  constructor(
    public rest:TicketRestService,
    private route: ActivatedRoute,
    private router: Router) {
    super();
  }

  customers:any = [];
  users:any = [];

  ngOnInit() {
    this.getCustomers();
    this.getUsers();
  }

  getCustomers() {
    this.customers = [];
    this.rest.getAllCustomers().subscribe((data: {}) => {

      this.customers = data;
    });
  }

  getUsers() {
    this.users = [];
    this.rest.getAllUsers().subscribe((data: {}) => {
      this.users = data;
    });
  }

  checkIfNewCustomer(value) {
    if (value == '-1') {
      this.router.navigate(['/customer-add/']);
    }
  }

  checkIfNewTechnical(value) {
    if (value == '-1') {
      this.router.navigate(['/user-add/']);
    }
  }

  addTicket() {
    this.rest.addTicket(this.ticketData).subscribe((result) => {
      if (result.error) {
        this.alert(result.error.message);
      } else {
        this.router.navigate(['/ticket-details/'+result.id]);
      }
    }, (err) => {
      alert(err.message)
      console.log(err.message);
    });
  }

}