import { Component, OnInit, Input } from '@angular/core';
import { CustomerRestService } from '../../rest-service/customer-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent extends MainComponent {

  @Input() customerData:any = { };

  constructor(public rest:CustomerRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
    this.rest.getCustomer(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.customerData = data;
    });
  }

  updateCustomer() {
    this.rest.updateCustomer(this.route.snapshot.params['id'], this.customerData).subscribe((result) => {
      if (result.error) {
        this.alert(result.error.message);
      } else {
        this.router.navigate(['/customer-details/'+result.id]);
      }
    }, (err) => {
      console.log(err);
    });
  }

}