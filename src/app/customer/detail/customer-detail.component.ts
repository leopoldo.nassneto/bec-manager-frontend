import { Component, OnInit } from '@angular/core';
import { CustomerRestService } from '../../rest-service/customer-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent extends MainComponent {

  customer:any;

  constructor(public rest:CustomerRestService, private route: ActivatedRoute, private router: Router) {
    super();
   }

  ngOnInit() {
    this.rest.getCustomer(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.customer = data;
    });
  }

}