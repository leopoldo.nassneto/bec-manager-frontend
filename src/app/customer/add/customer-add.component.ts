import { Component, OnInit, Input } from '@angular/core';
import { CustomerRestService } from '../../rest-service/customer-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.css']
})
export class CustomerAddComponent extends MainComponent {

  @Input() customerData = { name:'', email: ''};

  constructor(public rest:CustomerRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
  }

  addCustomer() {
    this.rest.addCustomer(this.customerData).subscribe((result) => {
      if (result.error) {
        this.alert(result.error.message);
      } else {
        this.router.navigate(['/customer-details/'+result.id]);
      }
    }, (err) => {
      console.log(err);
    });
  }

}