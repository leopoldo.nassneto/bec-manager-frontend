import { Component, OnInit } from '@angular/core';
import { CustomerRestService } from '../../rest-service/customer-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent extends MainComponent {

  customers:any = [];

  constructor(public rest:CustomerRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.customers = [];
    this.rest.getCustomers().subscribe((data: {}) => {
      console.log(data);
      this.customers = data;
    });
  }

  add() {
    this.router.navigate(['/customer-add']);
  }

  delete(id) {
    this.rest.deleteCustomer(id)
      .subscribe(res => {
          if (res.error) {
            this.alert(res.error.message);
          } else {
            this.getCustomers();
          }
        }, (err) => {
          console.log(err);
        }
      );
  }

}