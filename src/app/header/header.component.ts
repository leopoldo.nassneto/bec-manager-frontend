import { Component, OnInit } from '@angular/core';
import { UserRestService } from '../rest-service/user-rest-service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    
  }


  list_users() {
    this.router.navigate(['/users']);
  }
  add_user() {
    this.router.navigate(['/user-add']);
  }


}