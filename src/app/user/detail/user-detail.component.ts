import { Component, OnInit } from '@angular/core';
import { UserRestService } from '../../rest-service/user-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent extends MainComponent {

  user:any;

  constructor(public rest:UserRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
    this.rest.getUser(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.user = data;
    });
  }

}