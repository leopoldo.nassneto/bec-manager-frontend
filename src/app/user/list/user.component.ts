import { Component, OnInit } from '@angular/core';
import { UserRestService } from '../../rest-service/user-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent extends MainComponent {

  users:any = [];

  constructor(public rest:UserRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.users = [];
    this.rest.getUsers().subscribe((data: {}) => {
      console.log(data);
      this.users = data;
    });
  }

  add() {
    this.router.navigate(['/user-add']);
  }

  delete(id) {
    this.rest.deleteUser(id)
      .subscribe(res => {
          if (res.error) {
            this.alert(res.error.message);
          } else {
            this.getUsers();
          }
        }, (err) => {
          console.log(err);
        }
      );
  }

}