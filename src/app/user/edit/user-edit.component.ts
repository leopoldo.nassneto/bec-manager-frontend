import { Component, OnInit, Input } from '@angular/core';
import { UserRestService } from '../../rest-service/user-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent extends MainComponent {

  @Input() userData:any = { };

  constructor(public rest:UserRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
    this.rest.getUser(this.route.snapshot.params['id']).subscribe((data: {}) => {
      console.log(data);
      this.userData = data;
    });
  }

  updateUser() {
    this.rest.updateUser(this.route.snapshot.params['id'], this.userData).subscribe((result) => {
      if (result.error) {
        this.alert(result.error.message);
      } else {
        this.router.navigate(['/user-details/'+result.id]);
      }
    }, (err) => {
      console.log(err);
    });
  }

}