import { Component, OnInit, Input } from '@angular/core';
import { UserRestService } from '../../rest-service/user-rest-service';
import { ActivatedRoute, Router } from '@angular/router';
import { MainComponent } from '../../general/ManagerMainComponent'

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent extends MainComponent {

  @Input() userData = { name:'', email: ''};

  constructor(public rest:UserRestService, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit() {
  }

  addUser() {
    this.rest.addUser(this.userData).subscribe((result) => {
      if (result.error) {
        this.alert(result.error.message);
      } else {
        this.router.navigate(['/user-details/'+result.id]);
      }
    }, (err) => {
      console.log(err);
    });
  }

}