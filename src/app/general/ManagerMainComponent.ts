import { AlertsService } from 'angular-alert-module';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { OnInit } from '@angular/core';

export class MainComponent implements OnInit {

    protected alerts: AlertsService;

    constructor() {
        this.alerts = new AlertsService();
    }

    public alert(message) {
        alert(message);
        //not working yet :(
        //this.alerts.setMessage(message,'error');
    }

    ngOnInit() {}
}