import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

export const endpoint = 'http://localhost:80/ipa/api/v1/';
export const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(protected http: HttpClient) { }

  protected extractData(res: Response) {
    let body = res;
    return body || { };
  }

  protected handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(error as T);
    };
  }
}